# Youpytel

[Aplikace](http://rsp-youpytel.eu-gb.mybluemix.net/#/)

* Prezentace projektu: v repozitáři
* Projektový plán: v repozitáři - gant diagram.xls

* [Funkční a nefunkční požadavky](https://docs.google.com/document/d/117hAYWGHG71ejzm1n4gizj8SVrLXV94JTs9AmuW9074/edit?usp=sharing)
* [Use case diagram](https://www.lucidchart.com/invitations/accept/a56616cd-7723-44af-b330-e46f377bd7d6)
* [Klientská aplikace - repozitář](https://gitlab.fel.cvut.cz/tothpete/youpytel-client)
* [Risk log](https://docs.google.com/document/d/14jwa6GQOip0t8C1a5Q83nFmivU0AO1Xv-I3ww7_LqtU/edit?usp=sharing)
* [Testovací strategie](https://docs.google.com/document/d/1FBpoalWgEML_yG6EWqRuxSmaf8VaBRAR4_h3Wbm-Zw4/edit?usp=sharing)
* [Testovací scénáře](https://docs.google.com/document/d/1g6w7Fz31g3phS9ggQS-RLo5hPEULKBbnQsfsDfqolr8/edit?usp=sharing)
* [Wireframy / prototyp] (https://www.figma.com/file/bFabzLbJcDQdc3Jsq8o7K9Kb/YouPytel)